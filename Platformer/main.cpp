//
//  main.cpp
//  Platformer
//
//  Created by Andrew Voss on 5/18/18.
//  Copyright © 2018 Voss. All rights reserved.
//
#include <iostream>

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

// Here is a small helper for you! Have a look.
#include "ResourcePath.hpp"

#define xRes 800
#define yRes 600

#define frameRate 60

#define spriteX 32
#define spriteY 32

#define moveRate 5
#define gravityRate 3
#define jumpHieght 2 // 2x spriteY

sf::Sprite gravity(sf::Sprite enity){
    sf::Vector2f enityPos = enity.getPosition();
    float enityY = enityPos.y;
    if (enityY < yRes - spriteY){
        std::cout << enityY - gravityRate << std::endl;
        std::cout << yRes - spriteY << std::endl;
        if (enityY - gravityRate >= yRes - spriteY)
            enity.move(0, enityY - spriteY - yRes);
        else
            enity.move(0, gravityRate);
    } else {
        enity.move(0, yRes - spriteY - enityY);
    }
    return enity;
}

sf::Sprite jump(sf::Sprite enity){
    enity.move(0, -2 * spriteY);
    std::cout << "Pressed W" << std::endl;
    return enity;
}

int main(int, char const**)
{
    // Create the main window
    sf::RenderWindow window(sf::VideoMode(xRes, yRes), "Platformer0");
    window.setFramerateLimit(frameRate);
    
    sf::Event event;
    
    sf::Texture character0Texture;
    if (!character0Texture.loadFromFile(resourcePath() + "Character0_game.png")) {
        return EXIT_FAILURE;
    }
    
    sf::IntRect rectSourceSprite(0, 0, 32, 32);
    sf::Sprite character0(character0Texture, rectSourceSprite);
    
    character0.setPosition(0, yRes - spriteY);
    
    int position = 0;
    
    // Start the game loop
    while (window.isOpen()){
        character0 = gravity(character0);
        
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) && sf::Keyboard::isKeyPressed(sf::Keyboard::D))
            character0.move(moveRate, 0);
        
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)){
            std::cout << "Pressed A" << std::endl;
            position += 1;
            position %= 2;
            if (position){
                rectSourceSprite.left = 32;
            } else {
                rectSourceSprite.left = 64;
            }
            character0.setTextureRect(rectSourceSprite);
            character0.move(-moveRate, 0);
        }
        
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)){
            std::cout << "Pressed D" << std::endl;
            position += 1;
            position %= 2;
            if (position){
                rectSourceSprite.left = 32;
            } else {
                rectSourceSprite.left = 64;
            }
            character0.setTextureRect(rectSourceSprite);
            character0.move(moveRate, 0);
        }
        
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)){
            std::cout << character0.getPosition().y << std::endl;
            if (character0.getPosition().y == yRes - spriteY){
                character0 = jump(character0);
                std::cout << "from W" << std::endl;
            }
        }
    
        while (window.pollEvent(event)){
            if (event.type == sf::Event::EventType::Closed)
                window.close();
        }
        
        // Clear screen
        window.clear(sf::Color(255,255,255));
        
        window.draw(character0);
        
        // Update the window
        window.display();
    }
    
    return EXIT_SUCCESS;
}
